#!/bin/bash
WORK_DIR=$(pwd)

pip3 install safety

test -f "${WORK_DIR}"/requirements.txt && (safety check -r "${WORK_DIR}"/requirements.txt || (echo "Vulnerability found in python3 requirements" && exit 1))

# If the requirements dir doesn't exist then let's say our goodbyes
test -d "${WORK_DIR}"/requirements/ || exit 0

# Safety can't currently do a recursive lookup for linked requirements.txt so let's run agaisnt all teh filez
for file in "${WORK_DIR}"/requirements/*.txt
do
	safety check -r "${file}" || (echo "Vulnerability found in python3 requirements" && exit 1)
done
